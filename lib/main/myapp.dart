import 'package:eatziffy/view/homepage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

class MyApp extends GetWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'EatZiffy',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}
