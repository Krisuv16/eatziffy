import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:eatziffy/view/homepage.dart';
import 'package:flutter/material.dart';

class BottomNavigation extends StatefulWidget {
  final index;
  BottomNavigation(this.index);
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: PageView(
          physics: new NeverScrollableScrollPhysics(),
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _currentIndex = index);
          },
          children: <Widget>[
            HomePage(),
            HomePage(),
            HomePage(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() => _currentIndex = index);
          _pageController!.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
              title: Text(
                '',
                style: TextStyle(fontSize: 13),
              ),
              icon: Icon(Icons.home_filled),
              inactiveColor: Colors.black,
              activeColor: Colors.green),
          BottomNavyBarItem(
              title: Text(
                '',
                style: TextStyle(fontSize: 13),
              ),
              icon: Icon(Icons.add),
              inactiveColor: Colors.grey,
              activeColor: Color.fromRGBO(70, 209, 89, 1.0)),
          BottomNavyBarItem(
              title: Text(
                '',
                style: TextStyle(fontSize: 13),
              ),
              icon: Icon(Icons.star_border_outlined),
              inactiveColor: Colors.grey,
              activeColor: Color.fromRGBO(70, 209, 89, 1.0)),
        ],
      ),
    );
  }
}
