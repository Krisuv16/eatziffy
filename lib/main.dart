import 'package:eatziffy/controller/categories.controller.dart';
import 'package:eatziffy/controller/items.controller.dart';
import 'package:eatziffy/controller/label.controller.dart';
import 'package:eatziffy/controller/user.controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'main/myapp.dart';

Future<void> initialize() async {
  Get.lazyPut(() => UserController());
  Get.lazyPut(() => CategoriesController());
  Get.lazyPut(() => ItemController());
  Get.lazyPut(() => LabelController());
  await GetStorage.init();
}

void main() async {
  await dotenv.load(fileName: ".env");
  await initialize();
  runApp(MyApp());
}
