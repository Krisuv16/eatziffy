import 'package:eatziffy/config/network.dart';
import 'package:eatziffy/model/labels.model.dart';
import 'package:get/get.dart';

class LabelController extends GetxController {
  RxList<Label> label = <Label>[].obs;
  var labelIndex = 0.obs;
  Network api = Network();

  @override
  void onInit() {
    fetchLabel();
    super.onInit();
  }

  fetchLabel() async {
    try {
      var response = await api.fetch('labels');
      for (var list in response) {
        var listitems = Label.fromJson(list);
        label.add(listitems);
      }
    } catch (e) {
      throw e;
    }
  }
}
