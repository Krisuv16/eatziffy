import 'package:eatziffy/config/network.dart';
import 'package:eatziffy/model/items.model.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';

class ItemController extends GetxController {
  RxList<Items> items = <Items>[].obs;
  RxList<Items> itemwithid = <Items>[].obs;
  Network api = Network();
  final baseUrl = dotenv.env['API_URL']!;

  @override
  void onInit() {
    fetchItems();
    super.onInit();
  }

  fetchItems() async {
    try {
      var response = await api.fetch('items');
      items.clear();
      for (var item in response) {
        var list = Items.fromJson(item);
        items.add(list);
        items.refresh();
      }
    } catch (e) {
      throw e;
    }
  }

  fetchitemid(id) async {
    try {
      var response = await api.fetch("items?catId=$id");
      itemwithid.clear();
      for (var item in response) {
        var list = Items.fromJson(item);
        itemwithid.add(list);
        itemwithid.refresh();
      }
      print(itemwithid.length);
    } catch (e) {
      throw e;
    }
  }

  updateData(id, data) async {
    try {
      var response = await api.patchData('items/$id', data);
      Get.snackbar("Success", "Item Updated Sucessfully");
    } catch (e) {
      Get.snackbar("Failed", "Item Update Failed");
      throw e;
    }
  }

  deleteData(id) async{
        try {
      var response = await api.deleteData('items/$id');
      Get.snackbar("Success", "Item Deleted Sucessfully");
    } catch (e) {
      Get.snackbar("Failed", "Item Delete Failed");
      throw e;
    }
  }
}
