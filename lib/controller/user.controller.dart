import 'package:eatziffy/config/network.dart';
import 'package:eatziffy/model/user.model.dart';
import 'package:get/get.dart';

class UserController extends GetxController {
  RxList<User> user = <User>[].obs;
  Network api = Network();

  @override
  void onInit() {
    getUser();
    super.onInit();
  }

  getUser() async {
    try {
      var response = await api.fetch('userDetails');
      user.clear();
      for (var list in response) {
        var listmessage = User.fromJson(list);
        user.add(listmessage);
        user.refresh();
      }
    } catch (e) {
      throw e;
    }
  }
}
