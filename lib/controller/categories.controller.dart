import 'package:eatziffy/config/network.dart';
import 'package:eatziffy/model/categories.model.dart';
import 'package:get/get.dart';

class CategoriesController extends GetxController {
  RxList<Categories> categories = <Categories>[].obs;
  Rx<Categories> categorieswithid = Categories().obs;
  Network api = Network();
  var categoryid = 0.obs;
  var bookmarkcount = 0.obs;

  @override
  void onInit() {
    fetchCategoriesO();
    super.onInit();
  }

  fetchCategoriesO() async {
    try {
      var response = await api.fetch('categories');
      categories.clear();
      for (var list in response) {
        var categorylist = Categories.fromJson(list);
        categories.add(categorylist);
        categories.refresh();
      }
    } catch (e) {
      throw e;
    }
  }

  fetchCategoryid(id) async {
    try {
      var response = await api.fetch('categories/$id');
      categorieswithid.value = Categories.fromJson(response);
      bookmarkcount.value = categorieswithid.value.totalBookmarked!;
    } catch (e) {
      throw e;
    }
  }
}
