import 'package:eatziffy/constants.dart';
import 'package:eatziffy/controller/user.controller.dart';
import 'package:eatziffy/view/categories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

UserController userController = Get.find();

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(context),
            Padding(
              padding: const EdgeInsets.only(left: 23.0, top: 10, bottom: 10),
              child: Text(
                "Categories",
                style: GoogleFonts.mPlusRounded1c(
                    fontSize: 19, fontWeight: FontWeight.w700),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.63,
              width: MediaQuery.of(context).size.width,
              child: GridView.extent(
                maxCrossAxisExtent: 210.0,
                physics: NeverScrollableScrollPhysics(),
                primary: false,
                padding: const EdgeInsets.all(8),
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(183, 241, 217, 1),
                        borderRadius: BorderRadius.circular(20)),
                    padding: const EdgeInsets.all(8),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            maxRadius: 65,
                            backgroundColor: Colors.white,
                            child: Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/monitor.png"),
                                      fit: BoxFit.contain)),
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Expanded(
                            child: Text("Ideas",
                                style: GoogleFonts.mPlusRounded1c(
                                    color: Color.fromRGBO(43, 205, 137, 1),
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold)),
                          )
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => Get.to(Cooking()),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(251, 207, 207, 1),
                          borderRadius: BorderRadius.circular(20)),
                      padding: const EdgeInsets.all(8),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              maxRadius: 65,
                              backgroundColor: Colors.white,
                              child: Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/hamburger.png"),
                                        fit: BoxFit.contain)),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Expanded(
                              child: Text("Cooking",
                                  style: GoogleFonts.mPlusRounded1c(
                                      color: Color.fromRGBO(255, 112, 112, 1),
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(241, 239, 183, 1),
                        borderRadius: BorderRadius.circular(20)),
                    padding: const EdgeInsets.all(8),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            maxRadius: 65,
                            backgroundColor: Colors.white,
                            child: Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/monitor.png"),
                                      fit: BoxFit.contain)),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            child: Text("Programming",
                                style: GoogleFonts.mPlusRounded1c(
                                    color: Color.fromRGBO(205, 189, 43, 1),
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold)),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(207, 235, 251, 1),
                        borderRadius: BorderRadius.circular(20)),
                    padding: const EdgeInsets.all(8),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            maxRadius: 65,
                            backgroundColor: Colors.white,
                            child: Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/hamburger.png"),
                                      fit: BoxFit.contain)),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            child: Text("Music",
                                style: GoogleFonts.mPlusRounded1c(
                                    color: Color.fromRGBO(112, 135, 255, 1),
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold)),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Container header(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.31,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: kPrimaryOrange,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(34),
              bottomRight: Radius.circular(34))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 30,
          ),
          Align(
              alignment: Alignment.topRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        height: 8,
                        width: 8,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromRGBO(44, 243, 160, 1)),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image:
                                    AssetImage('assets/images/cil_bell.png'))),
                      ),
                    )
                  ],
                ),
              )),
          Obx(
            () => Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: userController.user.isEmpty
                  ? CupertinoActivityIndicator()
                  : Text(
                      "Hi" + " " + userController.user[0].fullName!,
                      style: GoogleFonts.openSans(
                          fontSize: 18, color: Colors.white),
                    ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text("Welcome Back !!!",
                style: GoogleFonts.mPlusRounded1c(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              decoration: BoxDecoration(
                  color: Color.fromRGBO(254, 219, 194, 0.56),
                  borderRadius: BorderRadius.circular(50)),
              child: new TextFormField(
                decoration: new InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide: BorderSide(color: Colors.transparent)),
                    hintText: 'What bookmarks are you searching for ?',
                    hintStyle: GoogleFonts.openSans(color: Colors.white),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusColor: Colors.transparent),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
