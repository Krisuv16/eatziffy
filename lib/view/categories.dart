import 'package:eatziffy/controller/categories.controller.dart';
import 'package:eatziffy/controller/items.controller.dart';
import 'package:eatziffy/controller/label.controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../constants.dart';

CategoriesController categoriesController = Get.find();
ItemController itemController = Get.find();
LabelController labelController = Get.find();

class Cooking extends StatelessWidget {
  const Cooking({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(context),
            labels(context),
            bookmarks(),
            main(context),
          ],
        ),
      ),
    );
  }

  Obx main(BuildContext context) {
    return Obx(() => categoriesController.categoryid.value == 0 ||
            labelController.labelIndex.value == 0
        ? allitems(context)
        : categoriesController.bookmarkcount.value == 0
            ? Container(
                height: MediaQuery.of(context).size.height * 0.5,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "No Bookmarks Found",
                    style: GoogleFonts.openSans(
                        fontSize: 20, color: Color.fromRGBO(252, 211, 181, 1)),
                  ),
                ),
              )
            : items(context));
    // return Obx(
    //   () => categoriesController.bookmarkcount.value == 0
    //       ? Container(
    //           height: MediaQuery.of(context).size.height * 0.5,
    //           width: MediaQuery.of(context).size.width,
    //           child: Center(
    //             child: Text(
    //               "No Bookmarks Found",
    //               style: GoogleFonts.openSans(
    //                   fontSize: 20, color: Color.fromRGBO(252, 211, 181, 1)),
    //             ),
    //           ),
    //         )
    //       : Obx(
    //           () => categoriesController.categoryid.value == 0
    //               ? allitems(context)
    //               : items(context),
    //         ),
    // );
  }

  Container allitems(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: itemController.items.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 110,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Card(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    itemController.items[index].image == null
                        ? CupertinoActivityIndicator()
                        : Card(
                            elevation: 2,
                            child: Container(
                              height: 58,
                              width: 60,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  image: DecorationImage(
                                    fit: BoxFit.contain,
                                    image: NetworkImage(
                                        "${itemController.items[index].image}"),
                                  )),
                            ),
                          ),
                    SizedBox(
                      width: 15,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            width: 120,
                            child: itemController.items[index].title == null
                                ? CupertinoActivityIndicator()
                                : Text(
                                    itemController.items[index].title!,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.mPlusRounded1c(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromRGBO(96, 81, 81, 1)),
                                  ),
                          ),
                          Row(
                            children: [
                              Obx(
                                () => GestureDetector(
                                  onTap: () {
                                    // var data = {
                                    //   "image": itemController
                                    //       .itemwithid[index].image,
                                    //   "title": itemController
                                    //       .itemwithid[index].title!,
                                    //   "isFavorite": itemController
                                    //       .itemwithid[index].isFavorite,
                                    //   "isBookmarked": itemController
                                    //               .itemwithid[index]
                                    //               .isBookmarked ==
                                    //           true
                                    //       ? false
                                    //       : true,
                                    //   "labelId": itemController
                                    //       .itemwithid[index].labelId,
                                    //   "catId": itemController
                                    //       .itemwithid[index].catId,
                                    //   "id": itemController
                                    //       .itemwithid[index].id
                                    // };
                                    // itemController.updateData(
                                    //     itemController.itemwithid[index].id,
                                    //     data);
                                    // itemController.fetchitemid(
                                    //     categoriesController
                                    //         .categoryid.value);
                                  },
                                  child: itemController
                                              .items[index].isBookmarked ==
                                          false
                                      ? Image.asset(
                                          "assets/images/bell_empt.png")
                                      : Image.asset(
                                          "assets/images/bell_fill.png"),
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Obx(
                                () => GestureDetector(
                                  onTap: () {
                                    // var data = {
                                    //   "image": itemController
                                    //       .itemwithid[index].image,
                                    //   "title": itemController
                                    //       .itemwithid[index].title!,
                                    //   "isFavorite": itemController
                                    //               .itemwithid[index]
                                    //               .isFavorite ==
                                    //           true
                                    //       ? false
                                    //       : true,
                                    //   "isBookmarked": itemController
                                    //       .itemwithid[index].isBookmarked,
                                    //   "labelId": itemController
                                    //       .itemwithid[index].labelId,
                                    //   "catId": itemController
                                    //       .itemwithid[index].catId,
                                    //   "id": itemController
                                    //       .itemwithid[index].id
                                    // };
                                    // itemController.updateData(
                                    //     itemController.itemwithid[index].id,
                                    //     data);
                                    // itemController.fetchitemid(
                                    //     categoriesController
                                    //         .categoryid.value);
                                  },
                                  child:
                                      itemController.items[index].isFavorite ==
                                              false
                                          ? Image.asset(
                                              "assets/images/star_empt.png")
                                          : Image.asset(
                                              "assets/images/star-fill.png"),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      child: Row(
                        children: [
                          CircleAvatar(
                            maxRadius: 25,
                            backgroundColor: Color.fromRGBO(44, 243, 160, 1),
                            child: Image.asset(
                                "assets/images/system-uicons_door-alt.png"),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            onTap: () {
                              // itemController.deleteData(
                              //     itemController.itemwithid[index].id);
                              // itemController.fetchitemid(
                              //     categoriesController.categoryid.value);
                            },
                            child: CircleAvatar(
                              maxRadius: 25,
                              backgroundColor: Color.fromRGBO(255, 112, 112, 1),
                              child:
                                  Image.asset("assets/images/lucide_trash.png"),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Padding items(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0, right: 15),
      child: Obx(
        () => Container(
          height: MediaQuery.of(context).size.height * 0.5,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            itemCount: itemController.itemwithid.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 110,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Card(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Card(
                          elevation: 2,
                          child: Container(
                            height: 58,
                            width: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: DecorationImage(
                                  fit: BoxFit.contain,
                                  image: NetworkImage(
                                      "${itemController.itemwithid[index].image!}"),
                                )),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                width: 120,
                                child: Text(
                                  itemController.itemwithid[index].title!,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: GoogleFonts.mPlusRounded1c(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromRGBO(96, 81, 81, 1)),
                                ),
                              ),
                              Row(
                                children: [
                                  Obx(
                                    () => GestureDetector(
                                      onTap: () {
                                        var data = {
                                          "image": itemController
                                              .itemwithid[index].image,
                                          "title": itemController
                                              .itemwithid[index].title!,
                                          "isFavorite": itemController
                                              .itemwithid[index].isFavorite,
                                          "isBookmarked": itemController
                                                      .itemwithid[index]
                                                      .isBookmarked ==
                                                  true
                                              ? false
                                              : true,
                                          "labelId": itemController
                                              .itemwithid[index].labelId,
                                          "catId": itemController
                                              .itemwithid[index].catId,
                                          "id": itemController
                                              .itemwithid[index].id
                                        };
                                        itemController.updateData(
                                            itemController.itemwithid[index].id,
                                            data);
                                        itemController.fetchitemid(
                                            categoriesController
                                                .categoryid.value);
                                      },
                                      child: itemController.itemwithid[index]
                                                  .isBookmarked ==
                                              false
                                          ? Image.asset(
                                              "assets/images/bell_empt.png")
                                          : Image.asset(
                                              "assets/images/bell_fill.png"),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Obx(
                                    () => GestureDetector(
                                      onTap: () {
                                        var data = {
                                          "image": itemController
                                              .itemwithid[index].image,
                                          "title": itemController
                                              .itemwithid[index].title!,
                                          "isFavorite": itemController
                                                      .itemwithid[index]
                                                      .isFavorite ==
                                                  true
                                              ? false
                                              : true,
                                          "isBookmarked": itemController
                                              .itemwithid[index].isBookmarked,
                                          "labelId": itemController
                                              .itemwithid[index].labelId,
                                          "catId": itemController
                                              .itemwithid[index].catId,
                                          "id": itemController
                                              .itemwithid[index].id
                                        };
                                        itemController.updateData(
                                            itemController.itemwithid[index].id,
                                            data);
                                        itemController.fetchitemid(
                                            categoriesController
                                                .categoryid.value);
                                      },
                                      child: itemController.itemwithid[index]
                                                  .isFavorite ==
                                              false
                                          ? Image.asset(
                                              "assets/images/star_empt.png")
                                          : Image.asset(
                                              "assets/images/star-fill.png"),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Spacer(),
                        Container(
                          child: Row(
                            children: [
                              CircleAvatar(
                                maxRadius: 25,
                                backgroundColor:
                                    Color.fromRGBO(44, 243, 160, 1),
                                child: Image.asset(
                                    "assets/images/system-uicons_door-alt.png"),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                onTap: () {
                                  itemController.deleteData(
                                      itemController.itemwithid[index].id);
                                  itemController.fetchitemid(
                                      categoriesController.categoryid.value);
                                },
                                child: CircleAvatar(
                                  maxRadius: 25,
                                  backgroundColor:
                                      Color.fromRGBO(255, 112, 112, 1),
                                  child: Image.asset(
                                      "assets/images/lucide_trash.png"),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Column labels(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 23.0, top: 10),
          child: Text(
            "Labels",
            style: GoogleFonts.mPlusRounded1c(
                fontSize: 19, fontWeight: FontWeight.w700),
          ),
        ),
        Obx(
          () => labelController.label.isEmpty
              ? CupertinoActivityIndicator()
              : Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      Chip(
                        backgroundColor: labelController.labelIndex.value == 0
                            ? kPrimaryOrange
                            : Color.fromRGBO(254, 186, 136, 0.6),
                        label: Icon(
                          Icons.add,
                          color: Colors.orange,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          itemController.fetchItems();
                          labelController.labelIndex.value = 0;
                        },
                        child: Chip(
                          backgroundColor: labelController.labelIndex.value == 0
                              ? kPrimaryOrange
                              : Color.fromRGBO(254, 186, 136, 0.6),
                          label: Text(
                            "All",
                            style: GoogleFonts.mPlusRounded1c(
                                fontSize: 12, color: Colors.white),
                          ),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: List.generate(
                                labelController.label.length,
                                (index) => Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: GestureDetector(
                                        onTap: () {
                                          labelController.labelIndex.value =
                                              labelController.label.indexOf(
                                                  labelController.label[index]);
                                          if (labelController
                                                  .label[index].catId ==
                                              null) {
                                            categoriesController
                                                .categoryid.value = 1;
                                          } else {
                                            categoriesController
                                                    .categoryid.value =
                                                labelController
                                                    .label[index].catId!;
                                          }
                                          categoriesController.fetchCategoryid(
                                              categoriesController
                                                  .categoryid.value);
                                          itemController.fetchitemid(
                                              categoriesController
                                                  .categoryid.value);
                                        },
                                        child: Chip(
                                          backgroundColor: labelController
                                                      .labelIndex.value ==
                                                  index
                                              ? kPrimaryOrange
                                              : Color.fromRGBO(
                                                  254, 186, 136, 0.6),
                                          label: Text(
                                            labelController.label[index].name!,
                                            style: GoogleFonts.mPlusRounded1c(
                                                fontSize: 12,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ],
    );
  }

  Padding bookmarks() {
    return Padding(
      padding: const EdgeInsets.only(left: 23.0, top: 10, right: 15),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Bookmarks",
                style: GoogleFonts.mPlusRounded1c(
                    fontSize: 19, fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Image.asset("assets/images/bi_bookmark.png"),
                  SizedBox(
                    width: 10,
                  ),
                  Obx(
                    () => Text(
                        categoriesController.bookmarkcount.value.toString()),
                  )
                ],
              )
            ],
          ),
          Spacer(),
          Image.asset("assets/images/fluent_arrow-swap-20-regular.png")
        ],
      ),
    );
  }

  Container header(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.31,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: kPrimaryOrange,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(34),
              bottomRight: Radius.circular(34))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.arrow_back_ios_outlined,
                    color: Colors.white,
                  ),
                ),
                Spacer(),
                Text(
                  "Cooking",
                  style: GoogleFonts.mPlusRounded1c(
                      fontSize: 22, color: Colors.white),
                ),
                Spacer(),
                Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.contain,
                          image: AssetImage('assets/images/cil_bell.png'))),
                )
              ],
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              decoration: BoxDecoration(
                  color: Color.fromRGBO(254, 219, 194, 0.56),
                  borderRadius: BorderRadius.circular(50)),
              child: new TextFormField(
                decoration: new InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide: BorderSide(color: Colors.transparent)),
                    hintText: 'What bookmarks are you searching for ?',
                    hintStyle: GoogleFonts.openSans(color: Colors.white),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusColor: Colors.transparent),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
