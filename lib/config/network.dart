import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart';

class Network {
  Client client = new Client();
  late Response response;
  final baseUrl = dotenv.env['API_URL']!;
  final GetStorage storage = GetStorage();

  fetch(uri) async {
    var fetchUri = Uri.parse("$baseUrl$uri");
    print(fetchUri);
    response = await client.get(fetchUri);
    if (response.statusCode != 200) {
      throw jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  deleteData(uri) async {
    var deleteDataUri = Uri.parse("$baseUrl$uri");
    try {
      response = await client.delete(deleteDataUri);
      if (response.statusCode != 202) {
        throw jsonDecode(response.body);
      }
    } catch (e) {
      return jsonDecode(response.body);
    }
  }

  patchData(uri, data) async {
    var patchDataUri = Uri.parse("$baseUrl$uri");
    response = await client.put(patchDataUri, body: jsonEncode(data));
    if (response.statusCode != 201 && response.statusCode != 200) {
      throw jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }
}
